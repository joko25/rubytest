require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_save_just_first_name
  	customer=Customer.new(:first_name =>"test")
 	assert_equal customer.valid?, true
    assert_equal customer.save, true 	
  end

  def test_save_just_last_name
  	customer=Customer.new(:last_name =>"test")
  	assert_equal customer.valid?, true
  	assert_equal customer.save, true
  end

  def test_save_all_field
  	customer=Customer.new(:first_name =>"test", :last_name =>"test")
  	assert_equal customer.valid?, true
  	assert_equal customer.save, true
  end

  def test_find_top_customer
   Customer.create([
     {:first_name => "share 1", :last_name => "test posting"}, {:first_name => "test2", :last_name => "test
      posting2"},
   ])
   assert_not_nil Customer.first.first_name
   assert_equal Customer.first.first_name, "Johny"
  end

  def test_relation_between_cusrtomer_and_credit
   customer = Customer.create(:first_name => "new_name", :last_name => "new name")
   assert_not_nil customer
   credit = Credit.create(:created => 1389618241, :paid => "true", :amount =>4900, :currency =>"usd", :refunded => "false", :customer_id=>customer.id, :disputed=>"false")
   assert_not_nil customer.credits
   assert_equal customer.credits.empty?, false
   assert_equal customer.credits[0].class, Credit
  end
end
