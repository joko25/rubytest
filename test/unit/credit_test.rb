require 'test_helper'

class CreditTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def test_relation_between_credit_and_customer
   credit = Credit.create(:created => 1389618241, :paid => "true", :amount =>4900, :currency =>"usd", :refunded => "false", :customer_id=>123, :disputed=>"false")
   assert_not_nil credit
   customer = Customer.create(:first_name => "new_name", :last_name => "new name")  
   assert_not_nil credit.customer
   assert_equal credit.customer.empty?, false
   assert_equal credit.customer[0].class, Customer
  end
end
