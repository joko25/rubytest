require 'rails_helper'

RSpec.describe Customer, :type => :model do
  it "orders by last name" do
    lindeman = Customer.create(:first_name => "Andy", :last_name => "Lindeman")
    chelimsky = Customer.create(:first_name => "David", :last_name => "Chelimsky")

    expect(Customer.ordered_by_last_name).to eq([chelimsky, lindeman])
  end
end
