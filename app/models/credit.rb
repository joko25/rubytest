class Credit < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_protected

  belongs_to :customer
  validates_inclusion_of  :currency,   
                          :in => %w( usd cents )
  validates :currency, :presence =>{:message =>"you must filled usd or cents"}
end
