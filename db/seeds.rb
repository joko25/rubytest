# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

customer=Customer.create([{:first_name =>"jhony", :last_name =>"Flow"},
						 {:first_name => "raj", :last_name => "jamnis"},
						 {:first_name => "andrew", :last_name => "chung"},
						 {:first_name => "mike", :last_name => "smith"}])



cus=Customer.find(1)
cus2=Customer.find(2)
cus3=Customer.find(3)
cus4=Customer.find(4)
#success
success1=cus.credits.create(:created=>"1389618241", :paid =>"true",:amount =>"4900", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success2=cus.credits.create(:created=>"1389618242", :paid =>"true",:amount =>"5900", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success3=cus.credits.create(:created=>"1389618243", :paid =>"true",:amount =>"4000", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success4=cus.credits.create(:created=>"1389618244", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success5=cus.credits.create(:created=>"1389618245", :paid =>"true",:amount =>"900", :currency =>"usd", :refunded =>"false", :disputed =>"false")

success6=cus2.credits.create(:created=>"1389618246", :paid =>"true",:amount =>"1900", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success7=cus2.credits.create(:created=>"1389618247", :paid =>"true",:amount =>"9000", :currency =>"usd", :refunded =>"false", :disputed =>"false")
success8=cus2.credits.create(:created=>"1389618248", :paid =>"true",:amount =>"3200", :currency =>"usd", :refunded =>"false", :disputed =>"false")

success9=cus3.credits.create(:created=>"1389618249", :paid =>"true",:amount =>"1200", :currency =>"usd", :refunded =>"false", :disputed =>"false")

success10=cus4.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"false")

#transactions that failed
failed1=cus3.credits.create(:created=>"13896182410", :paid =>"false",:amount =>"2900", :currency =>"usd", :refunded =>"true", :disputed =>"false")
failed2=cus3.credits.create(:created=>"13896182410", :paid =>"false",:amount =>"2900", :currency =>"usd", :refunded =>"true", :disputed =>"false")
failed3=cus3.credits.create(:created=>"13896182410", :paid =>"false",:amount =>"2900", :currency =>"usd", :refunded =>"true", :disputed =>"false")
failed4=cus4.credits.create(:created=>"13896182410", :paid =>"false",:amount =>"2900", :currency =>"usd", :refunded =>"true", :disputed =>"false")
failed5=cus4.credits.create(:created=>"13896182410", :paid =>"false",:amount =>"2900", :currency =>"usd", :refunded =>"true", :disputed =>"false")


#disputed:
disputed1=cus.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"true")
disputed2=cus.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"true")
disputed3=cus.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"true")
disputed4=cus2.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"true")
disputed5=cus2.credits.create(:created=>"13896182410", :paid =>"true",:amount =>"2900", :currency =>"usd", :refunded =>"false", :disputed =>"true")
