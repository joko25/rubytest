class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
	  t.integer :created
      t.string :paid
      t.integer :amount
      t.string :currency
      t.string :refunded
      t.integer :customer_id
      t.string :disputed
      t.timestamps
    end
  end
end
